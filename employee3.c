#include <stdio.h>
struct employee
{
     char employee_name[10];
     int employee_id;
     char doj[10];
     float salary;
};
typedef struct employee Employee;
Employee input()
{
      Employee e;
      printf("enter name of employee\n");
      scanf("%s",e.employee_name);
      printf("enter employee id\n");
      scanf("%d",&e.employee_id);
      printf("enter date of joining\n");
      scanf("%s",e.doj);
      printf("enter salary\n");
      scanf("%f",&e.salary);
      return e;
}
void output(Employee e)
{
       printf("employee name=%s\n",e.employee_name);
       printf("employee id=%d\n",e.employee_id);
       printf("date of joining=%s\n",e.doj);
       printf("salary=%f",e.salary);
}
int main()
{
       Employee e=input();
       output(e);
       return 0;
}       