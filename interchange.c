#include <stdio.h>
int input()
{
      int n;
      printf("enter array size\n");
      scanf("%d",&n);
      return n;
}
void input_array(int n,int a[n])
{
     for(int i=0;i<n;i++)
     {
         printf("enter array elements\n");
         scanf("%d",&a[i]);
     }
}
int compute(int n,int a[n],int *high,int *low,int *hpos,int *lpos)
{
    *hpos=0;
    *lpos=0;
    *high=a[0];
    for(int i=1;i<n;i++)
    {
       if(a[i]>*high)
       {
           *high=a[i];
           *hpos=i;
       }
     }
     *low=a[0];
     for(int j=1;j<n;j++)
     {
        if(a[j]<*low)
        {
           *low=a[j];
           *lpos=j;
        }
     }
 }
 void interchange(int a[],int *hpos,int *lpos)
 {
      int temp=a[*hpos];
      a[*hpos]=a[*lpos];
      a[*lpos]=temp;
 }
 void output(int n,int a[n])
 {
     for(int i=0;i<n;i++)
     {
        printf("res array=%d\n",a[i]);
     }
  }
  int main()
  {
       int n,*high,*low,*hpos,*lpos,m;
       n=input();
       int a[n];
       input_array(n,a);
       m=compute(n,a,&high,&low,&hpos,&lpos);
       interchange(a,&hpos,&lpos);
       output(n,a);
       return 0;
 }     
   
   
     
 
    