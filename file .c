#include <stdio.h>
int main()
{
     FILE *fa;
     printf("enter input data and enter ctrl.d\n");
     char c;
     fa=fopen("input.txt","w");
     c=getchar();
     while(c!=EOF)
     {
         putc(c,fa);
         c=getchar();
     }
     fclose(fa);
     printf("The output data is:\n");
     fa=fopen("input.txt","r");
     while((c=getc(fa))!=EOF)
     {
          printf("%c",c);
     }
     fclose(fa);
     return 0;
 }    
     