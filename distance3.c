#include <stdio.h>
#include <math.h>
struct point
{
      float x;
      float y;
};
typedef struct point Point;
Point input()
{
      Point p;
      printf("enter x coordinate\n");
      scanf("%f",&p.x);
      printf("enter y coordinate\n");
      scanf("%f",&p.y);
      return p;
}
float compute(Point p1,Point p2)
{
      float distance;
      distance=sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
      return distance;
}
void output(float distance)
{
       printf("distance between two points=%f",distance);
}
int main()
{
       float distance;
       Point p1,p2;
       p1=input();
       p2=input();
       distance=compute(p1,p2);
       output(distance);
       return 0;
} 



